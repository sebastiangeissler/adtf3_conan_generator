from conans import ConanFile, tools, CMake
from conans.errors import ConanException

class ADTF3ConanGenerator(ConanFile):
    name = "adtf3_conan_generator"
    description = "generates startup scripts and adtfenvironment file"

    url = "https://gitlab.com/digitalwerk/community/conan/adtf3_conan_generator.git"
    homepage = "https://www.digitalwerk.net/"

    license = "https://gitlab.com/digitalwerk/community/conan/adtf3_conan_generator/LICENSE.md"

    generators = "cmake"
    settings = {"os": ["Windows", "Linux"], "arch": ["x86_64", "armv8"], "compiler": ["Visual Studio", "gcc"], "build_type": ["Debug", "Release"]}

    scm = {
        "type": "git",
        "subfolder": ".",
        "url": "auto",
        "revision": "auto",
        "username": tools.get_env("CIP_JENKINS_USR"),
        "password": tools.get_env("CIP_JENKINS_PSW")
    }

    short_paths = True
    cmake = None
    keep_imports = True

    build_requires = "CMake/3.10.2@dw/stable"

    def build(self):
        self.cmake_factory()

        if self.should_build:
            self.cmake.build()

        if self.should_test:
            self.cmake.test()


    def package(self):
        self.cmake_factory()
        self.cmake.install()

    def cmake_factory(self):
        if not self.cmake:
            self.cmake = CMake(self)

            if self.settings.build_type=="Release":
                self.cmake.build_type = 'RelWithDebInfo'

            if self.package_folder:
                self.cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder

            self.cmake.configure()
