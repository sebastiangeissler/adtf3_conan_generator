def job_name_splitted = JOB_NAME.tokenize("/") as String[]
def job_type = job_name_splitted[-3]
def product = job_name_splitted[-4]
def project_folder = job_name_splitted[-5]
def builds_to_keep = "20"

def job_parameters
def compressed_file_ext
def build_info

pipeline {
    agent none
    libraries {
        lib("pipeline-lib@release/1.0.0")
    }
    options {
        skipDefaultCheckout()
        disableResume()
        timestamps()
        buildDiscarder(logRotator(numToKeepStr: builds_to_keep, artifactNumToKeepStr: builds_to_keep))
        copyArtifactPermission("${project_folder}/${product}/*")
    }
    parameters {
        string(defaultValue: "", name: "Platform", description: "", trim: true)
    }
    stages {
        stage("Initialization") {
            agent {
                label "master"
            }
            when {
                expression { currentBuild.getUpstreamBuilds() }
                beforeAgent true
            }
            steps {
                script {
                    copyArtifacts(
                        projectName: "${currentBuild.getUpstreamBuilds().first().getFullProjectName()}",
                        selector: specific("${currentBuild.getUpstreamBuilds().first().getNumber()}"),
                        filter: "parameters.json"
                    )
                    job_parameters = readJSON file: "parameters.json"
                }
            }
        }
        stage("Start building") {
            agent {
                label params["Platform"]
            }
            when {
                expression { currentBuild.getUpstreamBuilds() }
                beforeAgent true
            }
            environment {
                JENKINS_REPO_FOLDER = "${job_parameters['repo_folder']}"
                JENKINS_SCRIPTS_FOLDER = "${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}"
                JENKINS_CI_SETTINGS_FILE_PATH = "${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/jenkins/ci_settings.json"
                DW_CREDENTIALS_ID = "jenkins-ldap"
                CIP_CREDENTIALS_ID = "jenkins-cip"
                WORKSPACE = WORKSPACE.replace("\\", "/")
                CONAN_USER_HOME = "${WORKSPACE}/conan_home"
                CONAN_USER_HOME_SHORT = "${WORKSPACE}/cs"
                CONAN_USE_ALWAYS_SHORT_PATHS = "True" // Force short path on windows
                CONAN_LOG_RUN_TO_FILE = "True" // log to file
                CONAN_SCM_TO_CONANDATA = "True" // Don't export git user and password
                UPSTREAM_BUILD_NUMBER = currentBuild.getUpstreamBuilds().first().getNumber().toString()
            }
            stages {
                stage("Setup") {
                    steps{
                        script{
                            misc_tools.set_name_and_description(
                                upstream_project_build_number: "${UPSTREAM_BUILD_NUMBER}",
                                platform: params["Platform"]
                            )
                            misc_tools.clean_up_workspace()
                            dir(job_parameters["repo_folder"]) {
                                retry(2) {
                                    scmVars = checkout scm
                                }
                                env.GIT_COMMIT = scmVars.GIT_COMMIT
                            }
                            misc_tools.host_info()
                            misc_tools.subst_workspace()
                            build_info = Artifactory.newBuildInfo()
                            conan_tools.setup(
                                credentials_id: DW_CREDENTIALS_ID,
                            )
                            job_parameters["build_types"].each { build_type ->
                                sh script: "cp -r ${CONAN_USER_HOME} ${CONAN_USER_HOME}_${build_type.toLowerCase()}", label: "Copy conan home for concurrent build"
                            }
                            if (isUnix()) {
                                compressed_file_ext = "tgz"
                            } else {
                                compressed_file_ext = "zip"
                            }
                        }
                    }
                }
                stage("Build") {
                    steps {
                        script{
                            def build = [:]
                            job_parameters["build_types"].each { build_type ->
                                build[build_type] = {
                                    withEnv(["CONAN_USER_HOME=${WORKSPACE}/conan_home_${build_type.toLowerCase()}",
                                        "CONAN_USER_HOME_SHORT=${WORKSPACE}/cs${build_type.take(1).toLowerCase()}"]) {
                                        build_info.append(
                                            conan_tools.build_local(
                                                platform: params["Platform"],
                                                build_type: build_type,
                                                conan_file: "./${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/conan/build/conanfile.py",
                                                optional_conan_args: ""
                                            )
                                        )
                                    }
                                }
                            }
                            parallel build
                        }
                    }
                }
                stage("Prepeare Upload") {
                    steps {
                        script{
                            job_parameters["build_types"].each { build_type ->
                                sh script: """
                                    mkdir -p ./deploy_pkg
                                    cp -R ./conan_pkg/${build_type}/* ./deploy_pkg

                                    mkdir -p ./build-log/${build_type}
                                    cp conan_build/${build_type}/conan_run.log ./build-log/${build_type}/${build_type.toLowerCase()}-build.log
                                """ , label: "Join Debug and Release"
                            }
                        }
                    }
                }
                stage("Upload Artifacts") {
                    parallel {
                        stage("Conan") {
                            steps {
                                script {
                                    // Conan deployment
                                    build_info.append(conan_tools.export_pkg_to_cache(
                                        platform: params["Platform"],
                                        build_type: "Release",
                                        folder: "'${WORKSPACE}/deploy_pkg'",
                                        conan_file: "./${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/conan/deploy/conanfile.py",
                                        optional_conan_args: ""
                                    ))

                                    build_info.append(conan_tools.upload_pkg(
                                        conan_file: "./${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/conan/deploy/conanfile.py"
                                    ))

                                    // build info deployment
                                    build_info.name = product + " :: " + BRANCH_NAME.replaceAll('/', '-')
                                    build_info.number = "#${UPSTREAM_BUILD_NUMBER} - ${params['Platform']}"
                                    artifactory_tools.publish_build_info(build_info: build_info)
                                }
                            }
                        }
                        stage("Generic zip/tgz") {
                            steps {
                                script {
                                    // Generic zip deployment
                                    sh script: """
                                        cd ${WORKSPACE}/deploy_pkg
                                        rm -rf `cat ${WORKSPACE}/repo/scripts/conan/exclude-global.txt`
                                    """ , label: "Clean deploy pkg"

                                    [
                                        [folder: "./deploy_pkg", file: "${product}.${compressed_file_ext}"],
                                        [folder: "./build-log",  file: "${product}-${job_type}-log.${compressed_file_ext}"]
                                    ].each {
                                        misc_tools.compress_folder(folder: it.folder, file: it.file)
                                        artifactory_tools.upload_build_artifact(
                                            product_folder: product,
                                            file: it.file,
                                            platform: params["Platform"],
                                            build_nr: "${UPSTREAM_BUILD_NUMBER}",
                                            artifactory_credentials_id: DW_CREDENTIALS_ID
                                        )
                                    }

                                }
                            }
                        }
                    }
                }
            }
            post {
                success {
                    script{
                        archiveArtifacts(artifacts: "build-log/**/*.log", fingerprint: true)
                    }
                }
                cleanup {
                    script{
                        cleanWs(cleanWhenFailure: false, notFailBuild: true)
                        misc_tools.unsubst_workspace()
                    }
                }
            }
        }
    }
    post {
        always {
            script{
                if(!currentBuild.getUpstreamBuilds()) {
                    currentBuild.result = "ABORTED"
                    echo "This job has no upstream job, please start the product pipeline or rerun the last build"
                }
            }
        }
    }
}
