def job_name_splitted = JOB_NAME.tokenize("/") as String[]
def product = job_name_splitted[-3]
def project_folder = job_name_splitted[-4]
def builds_to_keep = "10"

def job_parameters = [:]
job_parameters["build_types"] = ["Release", "Debug"]
job_parameters["selected_platforms"] = []
job_parameters["scripts_folder"] = "scripts"
job_parameters["repo_folder"] = "repo"
job_parameters["windows_x64_platform"] = "WIN10_x64_vc141"
job_parameters["linux_x64_platform"] = "U1604_x64_gcc54"

def builder_jobs
def tester_jobs

pipeline {
    agent none
    libraries {
        lib("pipeline-lib@release/1.0.0")
    }
    options {
        skipDefaultCheckout()
        disableResume()
        timestamps()
        copyArtifactPermission("${project_folder}/${product}/*")
        buildDiscarder(logRotator(numToKeepStr: builds_to_keep, artifactNumToKeepStr: builds_to_keep))
    }
    parameters {
        booleanParam(defaultValue: true, description: job_parameters["windows_x64_platform"], name: "Windows x64")
        booleanParam(defaultValue: true, description: job_parameters["linux_x64_platform"], name: "Linux x64")
    }
    stages {
        stage("Initialization") {
            agent {
                label "master"
            }
            steps {
                script {
                    if (params["Windows x64"]) { job_parameters["selected_platforms"].add(job_parameters["windows_x64_platform"]) }
                    if (params["Linux x64"]) { job_parameters["selected_platforms"].add(job_parameters["linux_x64_platform"]) }

                    writeJSON(file: "parameters.json", json: job_parameters)
                    archiveArtifacts(artifacts: "parameters.json", fingerprint: true)
                }
            }
        }
        stage("Triggering Builds") {
            steps {
                script {
                    sub_jobs = [Builder: job_parameters["selected_platforms"]]

                    builder_jobs = misc_tools.trigger_sub_jobs(selected_sub_jobs: sub_jobs,
                                                               project_folder: project_folder,
                                                               product: product)

                    misc_tools.propagate_results(jobs: builder_jobs, fail_on_error: true)
                }
            }
        }
        stage("Triggering Deployment") {
            steps {
                script {
                    build(
                        job: misc_tools.generate_full_job_name(
                            project_folder: project_folder,
                            product: product,
                            job_name: "Deployer"),
                        wait: false
                    )
                }
            }
        }
        stage("Housekeeping"){
            agent {
                label job_parameters["linux_x64_platform"]
            }
            environment {
                JENKINS_REPO_FOLDER = "${job_parameters['repo_folder']}"
                JENKINS_SCRIPTS_FOLDER = "${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}"
                JENKINS_CI_SETTINGS_FILE_PATH = "${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/jenkins/ci_settings.json"
                DW_CREDENTIALS_ID = "jenkins-ldap"
                CIP_CREDENTIALS_ID = "jenkins-cip"
                CONAN_USER_HOME = "${WORKSPACE}/conan_home"
                CONAN_SCM_TO_CONANDATA = "True" // Don't export git user and password
            }
            steps {
                script {
                    misc_tools.clean_up_workspace()
                    dir("Artifacts"){
                        misc_tools.copy_sub_job_artifacts(jobs: builder_jobs)
                        misc_tools.analyse_build_logs(jobs: builder_jobs,
                                                        build_types: job_parameters["build_types"])
                    }
                    zip(archive: true, dir: "Artifacts", zipFile: "Artifacts.zip")

                    dir(job_parameters["repo_folder"]) {
                        retry(2) {
                            misc_tools.sparse_checkout_folder(path: "${job_parameters['scripts_folder']}", repo_credentials_id: DW_CREDENTIALS_ID)
                        }
                    }
                    conan_tools.setup(
                        credentials_id: DW_CREDENTIALS_ID,
                    )
                    artifactory_tools.remove_conan_pkgs_of_delted_branches(
                        conan_file: "./${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/conan/build/conanfile.py",
                        repo_credentials_id: DW_CREDENTIALS_ID, //TODO: Check credentials ID
                    )
                    artifactory_tools.remove_old_builds_from_artifactory(
                        product_folder: product,
                        artifactory_credentials_id: DW_CREDENTIALS_ID,
                        builds_to_keep: builds_to_keep
                    )
                    artifactory_tools.remove_artifacts_of_delted_branches(
                        product_folder: product,
                        artifactory_credentials_id: DW_CREDENTIALS_ID,
                        repo_credentials_id: DW_CREDENTIALS_ID //TODO: Check credentials ID
                    )
                }
            }
            post {
                cleanup {
                    script{
                        cleanWs(cleanWhenFailure: false, notFailBuild: true)
                    }
                }
            }
        }
    }
    post {
        success {
            script{
                misc_tools.job_status_mail(status: currentBuild.currentResult)
            }
        }
        failure {
            script{
                misc_tools.job_status_mail(status: currentBuild.currentResult)
            }
        }
        fixed {
            script{
                misc_tools.job_status_mail(status: "fixed")
            }
        }
    }
}
