def job_name_splitted = JOB_NAME.tokenize("/") as String[]
def product = job_name_splitted[-4]
def builds_to_keep = "10"

def force_release = false

def job_parameters
def deploy_environment

pipeline {
    agent none
    libraries {
        lib("pipeline-lib@release/1.0.0")
    }
    options {
        skipDefaultCheckout()
        disableResume()
        timestamps()
        buildDiscarder(logRotator(numToKeepStr: builds_to_keep, artifactNumToKeepStr: builds_to_keep))
    }
    stages {
        stage("Initialization") {
            agent {
                label "master"
            }
            when {
                expression { currentBuild.getUpstreamBuilds() }
                beforeAgent true
            }
            steps {
                script {
                    currentBuild.displayName = "#${currentBuild.getUpstreamBuilds().first().getNumber().toString()}"
                    copyArtifacts(
                        projectName: "${currentBuild.getUpstreamBuilds().first().getFullProjectName()}",
                        selector: specific("${currentBuild.getUpstreamBuilds().first().getNumber()}"),
                        filter: "parameters.json"
                    )
                    job_parameters = readJSON file: "parameters.json"

                    if (BRANCH_NAME ==~ /release\/.*/) {
                        deploy_environment = "staging"
                        misc_tools.check_for_locks()
                        milestone(1)
                    } else if (BRANCH_NAME ==~ /preview\/.*/) {
                        deploy_environment = "preview"
                    } else {
                        deploy_environment = "nightly"
                    }
                }
            }
        }
        stage("Force Deployment?") {
            when {
                expression { currentBuild.getUpstreamBuilds().first().getResult() != "SUCCESS" }
            }
            steps {
                script{
                    misc_tools.job_status_mail(status: "User input required")
                    input "One of the upstream jobs did not succeed, if you know what you are doing you can force the deployment"
                    force_release = true
                }
            }
        }
        stage("Deploy Staging Release / Nightly Build"){
            agent {
                label job_parameters["linux_x64_platform"]
            }
            environment {
                JENKINS_REPO_FOLDER = "${job_parameters['repo_folder']}"
                JENKINS_SCRIPTS_FOLDER = "${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}"
                JENKINS_CI_SETTINGS_FILE_PATH = "${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/jenkins/ci_settings.json"
                DW_CREDENTIALS_ID = "jenkins-ldap"
                CONAN_USER_HOME = "${WORKSPACE}/conan_home"
                CONAN_SCM_TO_CONANDATA = "True" // Don't export git user and password
                UPSTREAM_BUILD_NUMBER = currentBuild.getUpstreamBuilds().first().getNumber().toString()
            }
            when {
                allOf {
                    expression { currentBuild.getUpstreamBuilds() }
                    anyOf {
                        expression { force_release }
                        expression { currentBuild.getUpstreamBuilds().first().getResult() == "SUCCESS" }
                    }
                    anyOf {
                        branch pattern: "release\\/.*", comparator: "REGEXP"
                        branch pattern: "preview\\/.*", comparator: "REGEXP"
                        branch "master"
                    }
                }
                beforeAgent true
            }
            steps {
                script{
                    misc_tools.clean_up_workspace()
                    dir(job_parameters["repo_folder"]) {
                        retry(2) {
                            misc_tools.sparse_checkout_folder(path: "${job_parameters['scripts_folder']}", repo_credentials_id: DW_CREDENTIALS_ID)
                        }
                    }
                    conan_tools.setup(
                        credentials_id: DW_CREDENTIALS_ID,
                    )
                    def deploy = [:]
                    deploy["conan"] = {
                        artifactory_tools.promote_conan_pkgs(
                            selected_platforms: job_parameters["selected_platforms"],
                            deploy_env: deploy_environment,
                            conan_file: "./${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/conan/build/conanfile.py"
                        )
                    }
                    deploy["generic"] = {
                        artifactory_tools.promote_build_artifacts(
                            product_name: product,
                            product_folder: product,
                            selected_platforms: job_parameters["selected_platforms"],
                            deploy_env: deploy_environment,
                            build_nr: "${UPSTREAM_BUILD_NUMBER}",
                            artifactory_credentials_id: DW_CREDENTIALS_ID
                        )
                    }
                    parallel deploy
                }
            }
            post {
                cleanup {
                    script{
                        cleanWs(cleanWhenFailure: false, notFailBuild: true)
                    }
                }
            }
        }
        stage ("Deploy release to production"){
            when {
                allOf {
                    expression { currentBuild.getUpstreamBuilds() }
                    branch pattern: "release\\/.*", comparator: "REGEXP"
                    anyOf {
                        expression { force_release }
                        expression { currentBuild.getUpstreamBuilds().first().getResult() == "SUCCESS" }
                    }
                }
            }
            stages{
                stage("Confirm") {
                    steps {
                        script{
                            misc_tools.job_status_mail(status: "User input required")
                            input message "Do you want to deploy this release to the public production enviroment?"
                            milestone(2)
                        }
                    }
                }
                stage("Deploy Release") {
                    agent {
                        label job_parameters["linux_x64_platform"]
                    }
                    environment {
                        JENKINS_REPO_FOLDER = "${job_parameters['repo_folder']}"
                        JENKINS_SCRIPTS_FOLDER = "${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}"
                        JENKINS_CI_SETTINGS_FILE_PATH = "${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/jenkins/ci_settings.json"
                        DW_CREDENTIALS_ID = "jenkins-ldap"
                        CONAN_USER_HOME = "${WORKSPACE}/conan_home"
                        CONAN_SCM_TO_CONANDATA = "True" // Don't export git user and password
                        UPSTREAM_BUILD_NUMBER = currentBuild.getUpstreamBuilds().first().getNumber().toString()
                    }
                    steps {
                        script{
                            misc_tools.clean_up_workspace()
                            dir(job_parameters["repo_folder"]) {
                                retry(2) {
                                    misc_tools.sparse_checkout_folder(path: "${job_parameters['scripts_folder']}", repo_credentials_id: DW_CREDENTIALS_ID)
                                }
                            }
                            conan_tools.setup(
                                credentials_id: DW_CREDENTIALS_ID,
                            )
                            deploy_environment = "production"
                            def deploy = [:]
                            deploy["conan"] = {
                                artifactory_tools.promote_conan_pkgs(
                                    selected_platforms: job_parameters["selected_platforms"],
                                    deploy_env: deploy_environment,
                                    conan_file: "./${job_parameters['repo_folder']}/${job_parameters['scripts_folder']}/conan/build/conanfile.py"
                                )
                            }
                            deploy["generic"] = {
                                artifactory_tools.promote_build_artifacts(
                                    product_name: product,
                                    product_folder: product,
                                    selected_platforms: job_parameters["selected_platforms"],
                                    deploy_env: deploy_environment,
                                    build_nr: "${UPSTREAM_BUILD_NUMBER}",
                                    artifactory_credentials_id: DW_CREDENTIALS_ID
                                )
                            }
                            parallel deploy
                        }
                    }
                    post {
                        success {
                            script{
                                currentBuild.keepLog = true
                            }
                        }
                        cleanup {
                            script{
                                cleanWs(cleanWhenFailure: false, notFailBuild: true)
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            script{
                if(!currentBuild.getUpstreamBuilds()) {
                    currentBuild.result = "ABORTED"
                    echo "This job has no upstream job, please start the product pipeline or rerun the last"
                }
            }
        }
    }
}
